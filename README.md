## Group Name

Firemai

## Members

Pongsatorn Puangmeetham 602115017 ||
Poomrapee  Kanthapong   602115020

## Correspondence Email Addresses

Pongsatorn_pu@cmu.ac.th ||
Poomrapee_k@cmu.ac.th

## Url to Webpage

http://18.212.33.201:8081

## ip:port for Backend

http://18.212.33.201:8082 

## Katalon test repository

[link](https://gitlab.com/arm253489/project-katalon?fbclid=IwAR2ceBwVSFUnGzNEI4IfjULU_e8-1v-7ysZjfRQK0PSYuomoA2h-46Lg7X0) 

