package camt.se234.project.service;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SaleOrderServiceImplTest {
    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;
    static List<SaleOrder> mockSaleOrders;
    @Before
    public void setupDaoService() {
        orderDao = mock(OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);

        when(orderDao.getOrders()).thenReturn(mockSaleOrders);
    }

    @BeforeClass
    public static void setdata(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(001L).productId("445").name("RBD-445 Nana Aida").price(7777.00).description("Nana Aida เสียความทรงจำ").imageLocation(null).build());
        mockProducts.add(Product.builder().id(002L).productId("204").name("ADN-204 Kurea Hasumi").price(9999.00).description("Kurea Hasumi เหงาเกินแก้ขอแหย่สองที").imageLocation(null).build());
        mockProducts.add(Product.builder().id(003L).productId("625").name("MIDE-625 Tsubomi").price(1.50).description("เTsubomi จ้านายจอมซ่ากับเลขาจอมเปิ่น").imageLocation(null).build());
        mockProducts.add(Product.builder().id(004L).productId("211").name("ADN-211 Saeko Matsushita").price(11111111.11).description("Saeko Matsushita มะเร็งหายสายหื่น").imageLocation(null).build());

        List<SaleTransaction> mockSaleTransactions = new ArrayList<>();
        mockSaleTransactions.add(SaleTransaction.builder().id(100L).transactionId("111").product(mockProducts.get(0)).amount(10).build());
        mockSaleTransactions.add(SaleTransaction.builder().id(200L).transactionId("222").product(mockProducts.get(1)).amount(5).build());
        mockSaleTransactions.add(SaleTransaction.builder().id(300L).transactionId("333").product(mockProducts.get(2)).amount(50).build());
        mockSaleTransactions.add(SaleTransaction.builder().id(400L).transactionId("444").product(mockProducts.get(3)).amount(1).build());

        mockSaleOrders = new ArrayList<>();
        mockSaleOrders.add(SaleOrder.builder().id(999L).saleOrderId("001").transactions(
                new ArrayList<SaleTransaction>() {{ add(mockSaleTransactions.get(0));add(mockSaleTransactions.get(3)); }}).build());
        mockSaleOrders.add(SaleOrder.builder().id(555L).saleOrderId("002").transactions(
                new ArrayList<SaleTransaction>() {{ add(mockSaleTransactions.get(2));add(mockSaleTransactions.get(1)); }}).build());
    }

    @Test
    public void testgetSaleOrders(){
        assertThat(saleOrderService.getSaleOrders(),hasItems(
                mockSaleOrders.get(0),
                mockSaleOrders.get(1)
        ));
    }

    @Test
    public void testgetAverageSaleOrderPrice(){
        assertThat(saleOrderService.getAverageSaleOrderPrice(),is(5619475.555));
    }

}
