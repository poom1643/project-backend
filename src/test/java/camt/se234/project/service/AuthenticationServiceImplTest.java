package camt.se234.project.service;

import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.User;
import camt.se234.project.service.AuthenticationServiceImpl;
import org.junit.Before;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class AuthenticationServiceImplTest {

    UserDao userDao;
    AuthenticationServiceImpl authenticationService;


    @Before
    public void setupDaoService() {
        userDao = mock(UserDao.class);
        authenticationService = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
    }

    @Test
    public void testAuthenticateHaveUser() {
        User user1 = User.builder().id(234L).username("peepee").password("dojin").role("tester").build();
        when(userDao.getUser("peepee","dojin")).thenReturn(user1);
        assertThat(authenticationService.authenticate("peepee","dojin"),is(user1));
    }

    @Test
    public void testAuthenticateCheckUserInput() {
        User user1 = User.builder().id(234L).username("peepee").password("dojin").role("tester").build();
        when(userDao.getUser("peepee","dojin")).thenReturn(user1);
        assertThat(authenticationService.authenticate("arm","kung"),nullValue());
    }

}
