package camt.se234.project.service;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest{
    ProductDao productDao;
    ProductServiceImpl productService;

    @Before
    public void setup(){
        productDao = mock(ProductDao.class);
        productService = new ProductServiceImpl();
        productService.setProductDao(productDao);
    }

    @Test
    public void testgetAllProducts(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(001L).productId("445").name("RBD-445 Nana Aida").price(7777.00).description("Nana Aida เสียความทรงจำ").imageLocation(null).build());
        mockProducts.add(Product.builder().id(002L).productId("204").name("ADN-204 Kurea Hasumi").price(9999.00).description("Kurea Hasumi เหงาเกินแก้ขอแหย่สองที").imageLocation(null).build());
        mockProducts.add(Product.builder().id(003L).productId("625").name("MIDE-625 Tsubomi").price(-1.50).description("เTsubomi จ้านายจอมซ่ากับเลขาจอมเปิ่น").imageLocation(null).build());
        mockProducts.add(Product.builder().id(004L).productId("211").name("ADN-211 Saeko Matsushita").price(11111111.11).description("Saeko Matsushita มะเร็งหายสายหื่น").imageLocation(null).build());
        when(productDao.getProducts()).thenReturn(mockProducts);
        assertThat(productService.getAllProducts(), contains(mockProducts.toArray()));
    }

    @Test
    public void testgetAvailableProducts(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(001L).productId("445").name("RBD-445 Nana Aida").price(7777.00).description("Nana Aida เสียความทรงจำ").imageLocation(null).build());
        mockProducts.add(Product.builder().id(002L).productId("204").name("ADN-204 Kurea Hasumi").price(9999.00).description("Kurea Hasumi เหงาเกินแก้ขอแหย่สองที").imageLocation(null).build());
        mockProducts.add(Product.builder().id(003L).productId("625").name("MIDE-625 Tsubomi").price(-1.50).description("เTsubomi จ้านายจอมซ่ากับเลขาจอมเปิ่น").imageLocation(null).build());
        mockProducts.add(Product.builder().id(004L).productId("211").name("ADN-211 Saeko Matsushita").price(11111111.11).description("Saeko Matsushita มะเร็งหายสายหื่น").imageLocation(null).build());
        when(productDao.getProducts()).thenReturn(mockProducts);
        assertThat(productService.getAvailableProducts(), hasItems(
                Product.builder().id(001L).productId("445").name("RBD-445 Nana Aida").price(7777.00).description("Nana Aida เสียความทรงจำ").imageLocation(null).build(),
                Product.builder().id(002L).productId("204").name("ADN-204 Kurea Hasumi").price(9999.00).description("Kurea Hasumi เหงาเกินแก้ขอแหย่สองที").imageLocation(null).build(),
                Product.builder().id(004L).productId("211").name("ADN-211 Saeko Matsushita").price(11111111.11).description("Saeko Matsushita มะเร็งหายสายหื่น").imageLocation(null).build()
        ));


    }

    @Test
    public void testgetUnavailableProductSize(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(001L).productId("445").name("RBD-445 Nana Aida").price(7777.00).description("Nana Aida เสียความทรงจำ").imageLocation(null).build());
        mockProducts.add(Product.builder().id(002L).productId("204").name("ADN-204 Kurea Hasumi").price(9999.00).description("Kurea Hasumi เหงาเกินแก้ขอแหย่สองที").imageLocation(null).build());
        mockProducts.add(Product.builder().id(003L).productId("625").name("MIDE-625 Tsubomi").price(-1.50).description("เTsubomi จ้านายจอมซ่ากับเลขาจอมเปิ่น").imageLocation(null).build());
        mockProducts.add(Product.builder().id(004L).productId("211").name("ADN-211 Saeko Matsushita").price(11111111.11).description("Saeko Matsushita มะเร็งหายสายหื่น").imageLocation(null).build());
        when(productDao.getProducts()).thenReturn(mockProducts);
        assertThat(productService.getUnavailableProductSize(),is(1));
    }

}
